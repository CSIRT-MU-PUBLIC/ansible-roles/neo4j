# Ansible role - Neo4j - graph database #

## Role Description ##
This role serves for automatic installation of neo4j on debian-based systems.
Role also modifies neo4j configuration to enable support for user defined procedures (APOC).


## Requirements

This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

```yml
become: yes
```

## Example

Example of simple neo4j installation.

```yml
roles:
  - neo4j
```
